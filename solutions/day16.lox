import util;

var str = util.str;
var slice = util.slice;

class Pattern {
    init(index) {
        this.stage = 0;
        this.so_far = 1;  // skip the first thing
        this.repeats = index + 1;
    }

    next() {
        if (this.so_far == this.repeats) {
            this.stage = (this.stage + 1) % 4;
            this.so_far = 0;
        }
        this.so_far += 1;
        if (0 == this.stage) {
            return 0;
        }
        if (1 == this.stage) {
            return 1;
        }
        if (2 == this.stage) {
            return 0;
        }
        if (3 == this.stage) {
            return -1;
        }
        print "unknown stage! " + str(this.stage);
    }
}

fun next_digit(signal, pattern) {
    var total = 0;
    for (var i = 0; i < len(signal); i += 1) {
        total += pattern.next() * signal[i];
    }
    if (total < 0) {
        total *= -1;
    }
    return total % 10;
}

fun next_signal(signal) {
    var next = [nil] * len(signal);
    for (var i = 0; i < len(signal); i += 1) {
        next[i] = next_digit(signal, Pattern(i));
    }
    return next;
}

fun get_input() {
    var signal = input();
    var arr = [nil] * len(signal);
    for (var i = 0; i < len(signal); i += 1) {
        arr[i] = num(signal[i]);
    }
    return arr;
}

fun first(n, signal) {
    var out = "";
    for (var i = 0; i < n and i < len(signal); i += 1) {
        out += str(signal[i]);
    }
    return out;
}

fun part1() {
    var signal = get_input();
    for (var i = 0; i < 100; i += 1) {
        signal = next_signal(signal);
    }
    print first(8, signal);
}

fun part2() {
    var inp = get_input();
    var index = num(first(7, inp));
    var signal = slice(inp * 10000, index, len(inp) * 10000);

    for (var i = 0; i < 100; i += 1) {
        next_backpart(signal);
    }
    print first(8, signal);
}

fun next_backpart(signal) {
    /* Inspired by this thread: https://redd.it/ebf5cy.
    Mutates signal to get the next value in place.
    Relies on the observation that for the second half
    of the signal, each value is the sum of all values
    from itself to the end of the signal. */

    // index skips len(signal) - 1 because that is a constant.
    for (var i = len(signal) - 2; i >= 0; i -= 1) {
        signal[i] += signal[i + 1]; // contains sum of all following digits.
        signal[i] %= 10;
    }
}

part2();
