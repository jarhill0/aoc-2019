import list;
import set;
import map;

var List = list.ArrayList;
var Set = set.Set;
var Map = map.Map;

fun abs(x) {
    if (x < 0) {
        return -x;
    }
    return x;
}

class Point {
    init(x, y) {
        this.x = x;
        this.y = y;
    }

    dist() {
        return abs(this.x) + abs(this.y);
    }

    equals(other) {
        return this.x == other.x and this.y == other.y;
    }
}

class Path {
    init(path) {
        var point = Point(0,0);
        var point_map = Map();
        var point_list = List();
        var path_ind = 0;
        while (path_ind < len(path)) {
            var dir = path[path_ind];
            path_ind += 1;
            var mag = 0;
            while (path_ind < len(path) and path[path_ind] != ",") {
                mag = (mag * 10) + num(path[path_ind]);
                path_ind += 1;
            }
            path_ind += 1;

            var x_inc = 0;
            var y_inc = 0;
            if (dir == "R") {
                x_inc = 1;
            } else if (dir == "L") {
                x_inc = -1;
            } else if (dir == "U") {
                y_inc = 1;
            } else if (dir == "D") {
                y_inc = -1;
            } else {
                print "Unknown direction " + dir;
                return;
            }

            for (var i = 0; i < mag; i += 1) {
                point = Point(point.x + x_inc, point.y + y_inc);
                point_list.append(point);
                var sub_point_map = point_map.get(point.x);
                if (sub_point_map == nil) {
                    sub_point_map = Set();
                    point_map.put(point.x, sub_point_map);
                }
                sub_point_map.add(point.y);
            }
        }
        this.point_map = point_map;
        this.point_list = point_list;
    }

    contains_point(point) {
        var point_set = this.point_map.get(point.x);
        if (point_set != nil) {
            return point_set.contains(point.y);
        }
        return false;
    }

    wire_steps_to(point) {
        for (var i = 0; i < this.point_list.size; i += 1) {
            if (this.point_list.get(i).equals(point)) {
                return i + 1;  // corrects for origin not being in set.
            }
        }
        return false;
    }
}

fun part1() {
    var path1 = Path(input());
    var path2 = Path(input());

    var best = 100000000000000;
    for (var i = 0; i < path1.point_list.size; i += 1) {
        var point = path1.point_list.get(i);
        if (point.dist() < best and path2.contains_point(point)) {
            best = point.dist();
        }
    }
    print best;
}

fun part2() {
    var path1 = Path(input());
    var path2 = Path(input());

    var best = 100000000000000;
    for(var i = 0; i < path1.point_list.size and i < best; i += 1) {
        var point = path1.point_list.get(i);
        if (path2.contains_point(point)) {
            var total_dist = 1 + i + path2.wire_steps_to(point);
            if (total_dist < best) {
                best = total_dist;
            }
        }
    }
    print best;
}
