import util;
import map;

var HALTED = 0;
var INP_WAIT = 1;

class Program {
    init(prog) {
        this.prog = map.from_list(prog);
        this.pc = 0;
        this.rel_base = 0;
    }

    _store(dst, val) {
        this.prog.put(dst, val);
    }

    _lookup(ind) {
        var val = this.prog.get(ind);
        if (nil == val) {
            return 0;
        }
        return val;
    }

    _eval(ind, mode) {
        var val = this._lookup(ind);
        if (0 == mode) { // address mode
            return this._lookup(val);
        } else if (1 == mode) {  // immediate mode
            return val;
        } else if (2 == mode) {  // relative mode
            return this._lookup(val + this.rel_base);
        } else {
            print "unknown mode";
            print mode;
        }
    }

    _get_input() {
        return num(input());
    }

    _inp_and_status() {
        return [true, this._get_input()];
    }

    _output(thing) {
        print thing;
    }

    run() {
        // shortcuts
        var store = this._store;
        var pc = this.pc;
        var modes;

        fun eval(offset) {
            return this._eval(pc + offset, util.digit(modes, offset - 1));
        }

        fun dest(offset) {
            var mode = util.digit(modes, offset - 1);
            if (0 == mode) {
                return this._eval(pc + offset, 1);  // fake "immediate mode" for address
            } else if (2 == mode) {
                return this.rel_base + this._eval(pc + offset, 1);
            } else {
                print "unknown destination mode";
                print mode;
            }
        }

        while (true) {
            var operation = this._eval(pc, 1);
            var opcode = operation % 100;
            modes = util.floor(operation / 100);

            if (99 == opcode) {
                this.pc = pc;
                return HALTED;
            } else if (1 == opcode) {
                store(dest(3), eval(1) + eval(2));
                pc += 4;
            } else if (2 == opcode) {
                store(dest(3), eval(1) * eval(2));
                pc += 4;
            } else if (3 == opcode) {
                var inp_result = this._inp_and_status();
                if (inp_result[0]) {  // got input
                    store(dest(1), inp_result[1]);  // the actual result
                } else {
                    this.pc = pc;
                    return INP_WAIT;
                }
                pc += 2;
            } else if (4 == opcode) {
                this._output(eval(1));
                pc += 2;
            } else if (5 == opcode) {
                if (eval(1) != 0) {
                    pc = eval(2);
                } else {
                    pc += 3;
                }
            } else if (6 == opcode) {
                if (eval(1) == 0) {
                    pc = eval(2);
                } else {
                    pc += 3;
                }
            } else if (7 == opcode) {
                store(dest(3), num(eval(1) < eval(2)));
                pc += 4;
            } else if (8 == opcode) {
                store(dest(3), num(eval(1) == eval(2)));
                pc += 4;
            } else if (9 == opcode) {
                this.rel_base += eval(1);
                pc += 2;
            } else {
                print "Bad opcode!!";
                print opcode;
                print "pc:";
                print pc;
                return;
            }
        }
    }
}

class P1Program < Program {
    _get_input() {
        return 1;
    }
}

fun part1() {
    var inps = util.ints(input());
    for (var i = 0; i < 10000; i += 1) {
        inps.append(0);
    }
    var prog = P1Program(inps);
    prog.run();
}

class P2Program < Program {
    _get_input() {
        return 2;
    }
}

fun part2() {
    var inps = util.ints(input());
    for (var i = 0; i < 10000; i += 1) {
        inps.append(0);
    }
    var prog = P2Program(inps);
    prog.run();
}

part2();
