/**
 * My implementation of a HashSet.
 * Translated from Java implementation of a map!
 */

import list;

var some_primes = [11, 23, 47, 97, 197, 397, 797, 1597, 3203, 6421, 12853, 25717,
            51437, 102877, 205759, 411527, 823117, 1646237, 3292489, 6584983,
            13169977, 26339969, 52679969, 105359939, 210719881, 421439783,
            842879579, 1685759167];

class Entry {
    init(key, value) {
        this.key = key;
        this.value = value;
    }
}

class Map {
    init() {
        this.a = 67;
        this.b = 11;
        this.max_load_factor = 0.75;
        this.buckets = [nil] * 11;
        this.clear(); // initializes the buckets and sets size to 0
    }

    clear() {
        this._size = 0;

        for (var i = 0; i < len(this.buckets); i += 1) {
            this.buckets[i] = list.LinkedList();
        }
    }

    put(key, value) {
        var hash_value = this.hash(key);
        var bucket = this.buckets[hash_value];
        var bucket_it = bucket.iterator();
        while (bucket_it.has_next()) {
            var entry = bucket_it.next();
            if (entry.key == key) {
                var old_value = entry.value;
                entry.value = value;
                return old_value;
            }
        }
        // if we haven't returned already, then the key isn't already present in the map
        bucket.insert(0, Entry(key, value));
        this._size += 1;
        if (this.load_factor() > this.max_load_factor) {
            this.resize();
        }
        return nil;
    }

    get(key) {
        var hash_value = this.hash(key);
        var bucket_it = this.buckets[hash_value].iterator();
        while (bucket_it.has_next()) {
            var entry = bucket_it.next();
            if (key == entry.key) {
                return entry.value;
            }
        }
        // if we haven't returned already, then the key isn't present in the map
        return nil;
    }

    contains(key) {
        var hash_value = this.hash(key);
        var bucket_it = this.buckets[hash_value].iterator();
        while (bucket_it.has_next()) {
            if (key == bucket_it.next().key) {
                return true;
            }
        }
        // if we haven't returned already, then the key isn't present in the map
        return false;
    }

    size() {
        return this._size;
    }

    items() {
        var items = list.ArrayList();
        for (var i = 0; i < len(this.buckets); i += 1) {
            var bucket = this.buckets[i];
            var bucket_it = bucket.iterator();
            while (bucket_it.has_next()) {
                items.append(bucket_it.next());
            }
        }
        return items;
    }

    prt() {
        print "{";
        for (var it = this.items().iterator(); it.has_next();) {
            var next = it.next();
            print [next.key, next.value];
        }
        print "}";
    }

    load_factor() {
        return this.size() / len(this.buckets);
    }

    resize() {
        var it = this.items().iterator();
        this.buckets = [nil] * this.first_prime_above(2 * len(this.buckets));
        this.clear();

        while (it.has_next()) {
            var entry = it.next();
            this.put(entry.key, entry.value);
        }
    }

    hash(key) {
        return (this.a * (hash(key) + this.b)) % len(this.buckets);
    }

    first_prime_above(n) {
        for (var i = 0; i < len(some_primes); i += 1)
            if (some_primes[i] > n)
                return some_primes[i];
        return -1; // in practice, we won't get here...
    }
}

fun from_list(lst) {
    var map = Map();
    var i = 0;
    for (var it = lst.iterator(); it.has_next();) {
        map.put(i, it.next());
        i += 1;
    }
    return map;
}

class DefaultMap < Map {
    init(func) {
        super.init();
        this.func = func;
    }

    get(key) {
        if (this.contains(key)) {
            return super.get(key);
        }
        return this.func();
    }
}
