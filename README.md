# [Advent of Code 2019](https://adventofcode.com/2019) in [Lox](https://craftinginterpreters.com) with [custom extensions](https://gitlab.com/jarhill0/jlox)

[Advent of Code](https://adventofcode.com) is a series of programming
puzzles leading up to Christmas Day. This year I'm attempting to do them all
in Lox, which is a language built in the book
[*Crafting Interpreters* by Bob Nystrom](https://craftinginterpreters.com).
The language is barebones as far as scripting languages go, so I've been
extending [my version](https://gitlab.com/jarhill0/jlox) to add features so that
I can actually complete these puzzles.

My solutions are in the `solutions/` directory. That directory also contains
several data structures which I've implemented in Lox. In the root of the
repository is `runner.py`, a Python script for getting input from Advent
of Code and submitting answers.

On my system, I've aliased `lox` to run the jLox interpreter and `aoc` to run
`runner.py`.
